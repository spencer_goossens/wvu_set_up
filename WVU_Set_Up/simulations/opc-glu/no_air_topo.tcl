#!/usr/bin/tclsh

mol new OPC_GLUCOSE_No_Air.pdb autobonds no waitfor all

mol bondsrecalc top

topo retypebonds

topo bondtypenames

topo guessangles

topo guessdihedrals

mol reanalyze top

topo writelammpsdata Lammps_OPC_GLUCOSE_No_Air_data.imp full
