#!/usr/bin/tclsh

mol new TIP4P2005_GLUCOSE_Air.pdb autobonds no waitfor all

mol bondsrecalc top

topo retypebonds

topo bondtypenames

topo guessangles

topo guessdihedrals

mol reanalyze top

topo writelammpsdata Lammps_TIP4P2005_GLUCOSE_Air_data.imp full
